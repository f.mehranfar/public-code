﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Day1
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 4;
            double d = 4.0;
            string s = "HackerRank ";
            //-------------------
            //declare new variables
            //-------------------
            int varInt = 23;
            double varDbl = 6.5;
            string varStr = "is best for learning.";
            //-------------------
            //read from input
            //-------------------
            varInt = Convert.ToInt32(Console.ReadLine());
            varDbl = Convert.ToDouble(Console.ReadLine());
            varStr = Console.ReadLine();
            //-------------------
            //print output
            //-------------------
            varDbl = varDbl + d;
            Console.WriteLine(i+ varInt);
            Console.WriteLine(String.Format("{0:0.0}", varDbl));
            Console.WriteLine(s + varStr);
            //-------------------
            Console.ReadLine();
        }
    }
}
